(function () {

 /* en este archivo se configuran las rutas de la app, asi como sus controladores y vistas*/
  'use strict';
  const BASEURL = './scripts/';
  angular.module('core', ['ui.router', 'ngAria', 'ngMessages', 'ngAnimate', 'ngMaterial', 'md.data.table', 'ngMaterialDatePicker']).config(routeConfig);
  routeConfig.$inject = ['$stateProvider'];
  function routeConfig($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'scripts/templates/home.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
			.state('clientes',{
				url: '/clientes',
        abstract: true,
				templateUrl:'scripts/templates/clientes/clientes.html',
				controller: 'ClienteController',
				controllerAs: 'vm'
			})
      .state('clientes.listar', {
        url: '/listaClientes',
        templateUrl: 'scripts/templates/clientes/listaClientes.html',
        controller: 'ClienteController',
        controllerAs: 'vm'
      })
      .state('clientes.nuevo', {
        url: '/nuevo',
        templateUrl: 'scripts/templates/clientes/nuevoCliente.html',
        controller: 'ClienteController',
        controllerAs: 'vm'
      }).
      state('hoteles',{
        url: '/hoteles',
        abstract: true,
        templateUrl: 'scripts/templates/hoteles/hoteles.html',
        controller: 'HotelController',
        controllerAs: 'vm'
      })
      .state('hoteles.listar', {
        url: '/listar',
        templateUrl: 'scripts/templates/hoteles/listarHoteles.html',
        controller: 'HotelController',
        controllerAs: 'vm'
      })
      .state('hoteles.nuevo', {
        url: '/nuevo',
        templateUrl: 'scripts/templates/hoteles/nuevoHotel.html',
        controller: 'HotelController',
        controllerAs: 'vm'
      })
      .state('chofer', {
        url: '/chofer',
        abstract: true,
        templateUrl: 'scripts/templates/chofer/chofer.html',
        controller: 'ChoferController',
        controllerAs: 'vm'
      })
      .state('chofer.listar', {
        url: '/listar',
        templateUrl: 'scripts/templates/chofer/listarChofer.html',
        controller: 'ChoferController',
        controllerAs: 'vm'
      })
      .state('chofer.nuevo', {
        url: '/nuevo',
        templateUrl: 'scripts/templates/chofer/nuevoChofer.html',
        controller: 'ChoferController',
        controllerAs: 'vm'
      })
      .state('busetas', {
        url: '/busetas',
        abstract: true,
        templateUrl: 'scripts/templates/busetas/busetas.html',
        controller: 'BusetaController',
        controllerAs: 'vm'
      })
      .state('busetas.listar', {
        url: '/listar',
        templateUrl: 'scripts/templates/busetas/listarBusetas.html',
        controller: 'BusetaController',
        controllerAs: 'vm'
      })
      .state('busetas.nuevo', {
        url: '/nuevo',
        templateUrl: 'scripts/templates/busetas/nuevaBuseta.html',
        controller: 'BusetaController',
        controllerAs: 'vm'
      })
      .state('destino', {
        url: 'destino',
        abstract: true,
        templateUrl: 'scripts/templates/destino/destino.html',
        controller: 'DestinoController',
        controllerAs: 'vm'
      })
      .state('destino.nuevo', {
        ur: '/nuevo',
        templateUrl: 'scripts/templates/destino/nuevoDestino.html',
        controller: 'DestinoController',
        controllerAs: 'vm'
      })
      .state('destino.listar', {
        url: '/lista',
        templateUrl: 'scripts/templates/destino/listarDestino.html',
        controller: 'DestinoController',
        controllerAs: 'vm'
      })
      .state('reservaciones', {
        url: '/reservaciones',
        abstract: true,
        templateUrl: 'scripts/templates/reservaciones/reservaciones.html',
        controller: 'ReservacionesController',
        controllerAs: 'vm'
      })
      .state('reservaciones.nuevo', {
        url: '/nuevo',
        templateUrl: 'scripts/templates/reservaciones/nuevaReservacion.html',
        controller: 'ReservacionesController',
        controllerAs: 'vm'
      })
      .state('reservaciones.listar', {
        url: '/listar',
        templateUrl: 'scripts/templates/reservaciones/listarReservaciones.html',
        controller: 'ReservacionesController',
        controllerAs: 'vm'
      })
      .state('viajes', {
        url: '/viajes',
        abstract: true,
        templateUrl: 'scripts/templates/viajesRealizados/viajesRealizados.html',
        controller: 'ViajesRealizadosController',
        controllerAs: 'vm'
      })
      .state('viajes.listar', {
        url: '/viajes',
        templateUrl: 'scripts/templates/viajesRealizados/listarViajesRealizados.html',
        controller: 'ViajesRealizadosController',
        controllerAs: 'vm'
      })
  }
  angular.element(document).ready(function () {
		angular.bootstrap(document, ['core']);
  });

} ());
