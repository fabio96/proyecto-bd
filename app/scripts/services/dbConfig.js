(function(){
	'use strict';
	angular.module('core').factory('DatabaseConfig', databaseConfig);
	function databaseConfig(){
		return {
			user: '',
			password: '',
			server:  'Localhost',
			database: 'Reservacion',
			 port: 1433
		};
	}
}());
