(function () {
	'use strict';
	var mssql = require('mssql');
	angular.module('core').factory('DestinoService', DestinoService);
	DestinoService.$inject = ['DatabaseConfig', '$q'];
	function DestinoService(DatabaseConfig, $q) {
		function insertarDestino(destino) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('nombre', mssql.VarChar(20), destino.nombredestino)
					.input('horaAprox', mssql.Int, destino.horaAprox)
					.input('kiloAprox', mssql.Int, destino.kilometrosAprox)
					.execute('pa_insertarDestino')
					.then(function () {
						console.log('destino insertado');
						resolve();
					})
					.catch(function (err) {
						console.log('error al insertar', err);
						reject(err)
					});
				})
				.catch(function(err) {
					console.log('error al conectar', err);
					reject(err)
				});
			});

		}
		function obtenerTodos() {
			console.log('hola');
			return $q(function (resolve, reject) {
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.query('SELECT * FROM vw_informacion_destinos')
					.then(function(destinos){
						resolve(destinos);
						console.log(destinos);
					}).catch(function(err){
						console.log(err);
						reject(err);
					});
				})
				.catch(function(err) {
					console.log('error al conectar', err);
					reject(err);
				});
			});
		}
		function editarDestino(destino) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.Int, destino.idDestino)
					.input('nombre', mssql.VarChar(20), destino.nombredestino)
					.input('horaAprox', mssql.Int, destino.horaAprox)
					.input('kiloAprox', mssql.Int, destino.kilometrosAprox)
					.execute('pa_actualizarDestino')
					.then(function(){
						console.log('editado');
						resolve();
					})
					.catch(function(err){
						console.log('error al cargar', err);
						reject(err);
					});

				})
				.catch(function(err){
					console.log('error al conectar', err);
					reject(err);
				});
			});

		}
		function eliminarDestino(destino) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function(){
					new mssql.Request()
					.input('id', mssql.Int, destino.idDestino)
					.execute('pa_eliminarDestino')
					.then(function(){
						console.log('eliminado');
						resolve();
					})
					.catch(function(err){
						console.log('error al eliminar', err);
						reject(err);
					});
				})
				.catch(function(err){
					console.log('error al conectar', err)
					reject(err);
				});
			});

		}
		return {
			insertarDestino: insertarDestino,
			obtenerTodos: obtenerTodos,
			editarDestino: editarDestino,
			eliminarDestino: eliminarDestino
		};
	}
}())
