(function () {
	var mssql = require('mssql');
	'use strict';
	angular.module('core').factory('HotelService', HotelService);
	HotelService.$inject = ['DatabaseConfig', '$q'];
	function HotelService(DatabaseConfig, $q) {
		function insertarHotel(hotel) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
						.input('nombre', mssql.VarChar(50), hotel.nombreHotel)
						.input('ciudad', mssql.VarChar(50), hotel.ciudad)
						.input('direccion', mssql.VarChar(50), hotel.direccion)
						.input('telefono', mssql.VarChar(50), hotel.telefono)
						.input('correo', mssql.VarChar(50), hotel.correoHotel)
						.execute('pa_insertarHotel')
						.then(function (nada) {
							console.log(nada);
							resolve();
						})
						.catch(function (err) {
							console.log('error', err);
							reject(err);
						})
				})
				.catch(function (err) {
					alert('Hubo un error', err);
					console.log('esa no me la esperaba', err);
				});
			});
		}
		function obtenerTodos() {
			return $q(function (resolve, reject) {
				mssql.connect(DatabaseConfig)
					.then(function () {
						new mssql.Request()
							.query('select * from vw_informacion_hoteles')
							.then(function (hoteles) {
								resolve(hoteles);
							}).catch(function (err) {
								reject(err);
							});
					})
					.catch(function (err) {
						reject(err);
					});
			});
		}
		function actualizarHotel(hotel) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
						.input('id', mssql.Int, parseInt(hotel.idHotel))
						.input('nombre', mssql.VarChar(50), hotel.nombreHotel)
						.input('ciudad', mssql.VarChar(50), hotel.ciudad)
						.input('direccion', mssql.VarChar(50), hotel.direccion)
						.input('telefono', mssql.VarChar(50), hotel.telefono)
						.input('correo', mssql.VarChar(50), hotel.correoHotel)
						.execute('pa_actualizarHotel')
						.then(function () {
							console.log('editado');
							resolve()
						})
						.catch(function (err) {
							console.log('hubo un error', err);
							reject(err);
						});
				})
				.catch(function (err) {
					console.log('acaso no la viste venir >:v', err);
					reject(err);
				});
			});
		}
		function eliminarHotel(hotel) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
						.input('id', mssql.Int, parseInt(hotel.idHotel))
						.execute('pa_eliminarHotel')
						.then(function (nada) {
							console.log(nada);
							resolve(nada);
						})
						.catch(function (err) {
							console.log('Hubo un error', err);
							reject(err);
						});
				})
				.catch(function (err) {
					console.log('un error rufian', err);
					reject(err);
				});
			});
		}
		return {
			insertarHotel: insertarHotel,
			obtenerTodos: obtenerTodos,
			actualizarHotel: actualizarHotel,
			eliminarHotel: eliminarHotel
		}
	}
} ());