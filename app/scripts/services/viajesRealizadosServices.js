(function(){
  'use strict';
  var mssql = require('mssql');
  angular.module('core').service('ViajesRealizadosService', ViajesRealizadosService);
  ViajesRealizadosService.$inject = ['DatabaseConfig', '$q'];
  function ViajesRealizadosService(DatabaseConfig, $q){
    function obtenerTodos(){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .query('SELECT * FROM vw_viajes_realizados')
          .then(function(viajes){
            resolve(viajes);
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function insertarViaje(viaje){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('idReserva', mssql.Int, viaje.idReservacion)
          .input('tipoPago', mssql.VarChar(20), viaje.tipoDePago)
          .input('fechaHoraLlegad', mssql.DateTime, viaje.fechaHoraLlegada)
          .input('kilomSalida', mssql.VarChar(20), viaje.kilomSalida)
          .input('kilomLlegada', mssql.VarChar(20), viaje.kilomLlegada)
          .execute('pa_insertarViajeRealizado')
          .then(function(){
            resolve();
          })
          .catch(function(err){
              reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function eliminarViaje(viaje){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('id', mssql.Int, viaje.idViajesRealizados)
          .execute('pa_eliminarViajeRealizado')
          .then(function(){
            resolve();
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function actualizarViaje(viaje){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('id', mssql.Int, viaje.idViajesRealizados)
          .input('tipoPago', mssql.VarChar(20), viaje.tipoDePago)
          .input('fechaHoraLlegad', mssql.DateTime, viaje.fechaHoraLlegada)
          .input('kilomSalida', mssql.VarChar(20), viaje.kilometrajeSalida)
          .input('kilomLlegada', mssql.VarChar(20), viaje.kilometrajeLlegada)
          .execute('pa_actualizarViajeRealizado')
          .then(function(){
            resolve();
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function buscarViaje(viaje){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('id', mssql.Int, viaje.idViajesRealizados)
          .execute('pa_buscar_viaje_realizado')
          .then(function(viaje){
            resolve(viaje);
          })
          .catch(function(err){
            reject(err)
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    return{
      insertarViaje: insertarViaje,
      actualizarViaje: actualizarViaje,
      eliminarViaje: eliminarViaje,
      obtenerTodos: obtenerTodos,
      buscarViaje: buscarViaje
    };
  }
}());
