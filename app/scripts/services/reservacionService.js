(function(){
  'use strict';
  var mssql = require('mssql');
  angular.module('core').service('ReservacionService', ReservacionService);
  ReservacionService.$inject = ['DatabaseConfig','$q'];
  function ReservacionService(DatabaseConfig, $q){
    function obtenerPendientes(){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .query('SELECT * FROM vw_reservaciones_pendientes')
          .then(function(reservaciones){
            resolve(reservaciones);
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function obtenerDeHoy(){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .query('SELECT * FROM vw_reservaciones_de_hoy')
          .then(function(reservaciones){
            resolve(reservaciones);
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      })
    }
    function obtenerTodos(){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .query('SELECT * FROM vw_todas_reservaciones_')
          .then(function(reservaciones){
            resolve(reservaciones);
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function insertarReservacion(reservacion){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('cantidad', mssql.Int, reservacion.cantidadPersonas)
          .input('fechaHorSalida', mssql.DateTime, reservacion.fechaHoraSalida)
          .input('cliente', mssql.VarChar(20), reservacion.idCliente)
          .input('chofer', mssql.VarChar(20), reservacion.idChofer)
          .input('buseta', mssql.VarChar(20), reservacion.placaBuseta)
          .input('destino', mssql.Int, reservacion.idDestino)
          .input('idHotel', mssql.Int, reservacion.idHotel)
          .input('precio', mssql.VarChar(20), reservacion.precio)
          .execute('pa_insertarReservacion')
          .then(function(reservacion){
            resolve(reservacion);
          })
          .catch(function(err){
            reject(err)
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function actualizarReservacion(reservacion){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('id', mssql.Int, reservacion.idReservacion)
          .input('cantidad', mssql.Int, reservacion.cantidadPersonas)
          .input('fechaHorSalida', mssql.DateTime, reservacion.fechaHoraSalida)
          .input('estado', mssql.VarChar(20), reservacion.estado)
          .input('cliente', mssql.VarChar(20), reservacion.cliente)
          .input('chofer', mssql.VarChar(20), reservacion.chofer)
          .input('buseta', mssql.VarChar(20), reservacion.buseta)
          .input('destino', mssql.Int, reservacion.destino)
          .input('idHotel', mssql.Int, reservacion.idHotel)
          .input('precio', mssql.Int, reservacion.precio)
          .execute('pa_actualizarReservacion')
          .then(function(reservacion){
            resolve(reservacion);
          })
          .catch(function(err){
            console.log('aqui el error '+ err);
            reject(reservacion)
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }
    function eliminarReservacion(reservacion){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('id', mssql.Int, reservacion.idReservacion)
          .execute('pa_eliminarReservacion')
          .then(function(){
            resolve();
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err)
        });
      });
    }

    function buscarReservacion(reservacion){
      return $q(function(resolve, reject){
        mssql.connect(DatabaseConfig)
        .then(function(){
          new mssql.Request()
          .input('idReservacion', mssql.Int, reservacion.idReservacion)
          .execute('pa_buscar_reservacion')
          .then(function(reservacion){
            resolve(reservacion);
          })
          .catch(function(err){
            reject(err);
          });
        })
        .catch(function(err){
          reject(err);
        });
      });
    }



    return {
      obtenerTodos: obtenerTodos,
      obtenerDeHoy: obtenerDeHoy,
      obtenerPendientes, obtenerPendientes,
      insertarReservacion: insertarReservacion,
      actualizarReservacion: actualizarReservacion,
      eliminarReservacion: eliminarReservacion,
      buscarReservacion: buscarReservacion
    }
  }
}())
