
(function () {
	'use strict';
	var mssql = require('mssql');
	angular.module('core').factory('BusetaService', BusetaService);
	BusetaService.$inject = ['DatabaseConfig', '$q'];
	function BusetaService(DatabaseConfig, $q) {
		function insertarBuseta(buseta) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('placa', mssql.VarChar(20), buseta.placaBuseta)
					.input('capacidad', mssql.Int, buseta.capacidadPersonas)
					.input('marca', mssql.VarChar(20), buseta.marca)
					.input('modelo', mssql.VarChar(20), buseta.modelo)
					.input('annoCompra', mssql.Date(20), buseta.annoCompra)
					.input('fechaVenceRTV', mssql.Date(20), buseta.fechaVenceRTV)
					.execute('pa_insertarBuseta')
					.then(function (buseta) {
						console.log('buseta insertada');
						resolve(buseta);
					}).catch(function (err) {
						console.log(err);
						reject(err);
					});
				})
				.catch(function (err) {
					console.log('errr', err);
					reject(err);
				});
			})
		}
		function obtenerTodas() {
			return $q(function (resolve, reject) {
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.query('SELECT * FROM Buseta')
					.then(function (busetas) {
						resolve(busetas);
					}).catch(function (err) { reject(err); });
				})
				.catch(function (err) {
					reject(err);
				});
			});
		}
		function editarBuseta(buseta) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('placa', mssql.VarChar(20), buseta.placaBuseta)
					.input('capacidad', mssql.Int, buseta.capacidadPersonas)
					.input('marca', mssql.VarChar(20), buseta.marca)
					.input('modelo', mssql.VarChar(20), buseta.modelo)
					.input('annoCompra', mssql.Date(20), buseta.annoCompra)
					.input('fechaVenceRTV', mssql.Date(20), buseta.fechaVenceRTV)
					.execute('pa_actualizarBusetas')
					.then(function () {
						console.log('editado');
						resolve();
					})
					.catch(function (err) {
						console.log('error :v', err);
						reject(err);
					});

				})
				.catch(function (err) {
					console.log('error en con', err);
					reject(err);
				});
			});

		}
		function eliminarBuseta(buseta) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('placa', buseta.placaBuseta)
					.execute('pa_eliminarBuseta')
					.then(function () {
						console.log('eliminada');
						resolve();
					})
					.catch(function (err) {
						console.log('error eliminar', err);
						reject(err);
					});
				})
				.catch(function (err) {
					console.log('error con', err);
					reject(err);
				});
			});
		}
		return {
			insertarBuseta: insertarBuseta,
			obtenerTodas: obtenerTodas,
			editarBuseta: editarBuseta,
			eliminarBuseta: eliminarBuseta
		};
	}
}());
