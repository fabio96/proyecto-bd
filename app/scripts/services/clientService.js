(function () {
	'use strict';
	var mssql = require('mssql');
	angular.module('core').factory('ClientService', ClientService);
	ClientService.$inject = ['DatabaseConfig', '$q'];
	function ClientService(DatabaseConfig, $q) {
		function insertarCliente(cliente) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.VarChar(50), cliente.idCliente)
					.input('nombre', mssql.VarChar(50), cliente.nombreCliente)
					.input('telefono', mssql.VarChar(50), cliente.telefono)
					.input('habitacion', mssql.Int, parseInt(cliente.numeroHabitacion))
					.input('email', mssql.VarChar(50), cliente.correoCliente)
					.execute('pa_insertarCliente').then(function (cliente) {
						console.log(cliente);
						resolve(cliente);
					}).catch(function (err) {
						console.log('hubo un error :(', err)
						reject(err);
					});
				})
				.catch(function (err) {
					alert('Hubo un error al cargar');
					console.log('hubo un error papu', err);
					reject(err);
				});
			})
		};
		function obtenerTodos() {
			return $q(function (resolve, reject) {
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.query('SELECT * FROM wv_informacion_clientes')
					.then(function (clientes) {
						resolve(clientes);
					}).catch(function (err) {
						reject(err);
						console.log('el erroe esta aqui');
					})
				}).catch(function (err) {
					reject(err);
				});
			});
		};
		function editarCliente(cliente) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.VarChar(50), cliente.idCliente)
					.input('nombre', mssql.VarChar(50), cliente.nombreCliente)
					.input('telefono', mssql.VarChar(50), cliente.telefono)
					.input('numHabitacion', mssql.Int, cliente.numeroHabitacion)
					.input('correo', mssql.VarChar(50), cliente.correoCliente)
					.execute('pa_actualizarClientes').then(function () {
					resolve();
					console.log('editado');
					}).catch(function (err) {
						console.log(err);
						reject(err);
					});
				})
				.catch(function (err) {
					reject();
					console.log(err);
				});
			})
		}
		function eliminarCliente(cliente) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.VarChar(50), cliente.idCliente)
					.execute('pa_eliminarCliente')
					.then(function (cliente) {
						resolve(cliente);
						console.log('Eliminado', cliente);
					}).catch(function (err) {
						console.log(err);
						reject(err);
					});
				})
				.catch(function (err) {
					console.log(err);
					reject(err);
				});
			});
		}
		function buscarCliente(cliente){
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function(){
					new mssql.Request()
					.input('id', mssql.VarChar(20), cliente.idCliente)
					.execute('pa_buscar_cliente')
					.then(function(cliente){
						resolve(cliente);
					})
					.catch(function(err){
						reject(err);
					});
				})
				.catch(function(err){
					reject(err);
				});
			});
		}
		return {
			insertarCliente: insertarCliente,
			obtenerTodos: obtenerTodos,
			eliminarCliente: eliminarCliente,
			editarCliente: editarCliente,
			buscarCliente: buscarCliente
		}
	}
} ());
