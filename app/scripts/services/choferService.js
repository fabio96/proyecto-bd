(function () {
	'use strict';
	var mssql = require('mssql');
	angular.module('core').factory('ChoferService', ChoferService);
	ChoferService.$inject = ['DatabaseConfig', '$q'];
	function ChoferService(DatabaseConfig, $q) {

		function buscar(chofer){
			mssql.connect(DatabaseConfig)
			.then(function(){
				new mssql.Request()
				.input('nombre', mssql.VarChar(50), chofer.nombre)
				.execute('pa_encontrarChofer')
				.then(function(nose){
					console.log(nose);
					mssql.close();
				})
				.catch(function(err){
					console.log(err);
				});
			})
			.catch(function(err){
				alert('lo que me faltaba, un error');
				console.log('ahora no por favor', err);
			});
		}
		function insertarChofer(chofer) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.VarChar(50), chofer.idChofer)
					.input('nombre', mssql.VarChar(50), chofer.nombreChofer)
					.input('telefono', mssql.VarChar(50), chofer.telefono)
					.input('direccion', mssql.VarChar(50), chofer.direccion)
					.input('tipoLicencia', mssql.VarChar(50), chofer.tipoLicencia)
					.input('fechaVenceLicencia', mssql.DateTime(50), chofer.fechaVenceLicencia)
					.execute('pa_insertarChofer')
					.then(function (nocBuenoSiSePeroNoTeWuaDecir) {
						console.log(nocBuenoSiSePeroNoTeWuaDecir);
						resolve();
					})
					.catch(function (err) {
						console.log('Esa no me la esperaba, un error', err);
						reject(err);
					});
				})
				.catch(function (err) {
					console.log('Esa no me la esperaba, un error', err);
					reject(err);
				});
			});
		}
		function obtenerTodos() {
			return $q(function (resolve, reject) {
				mssql.connect(DatabaseConfig)
					.then(function () {
							new mssql.Request()
							.query('SELECT * FROM vw_informacion_choferes')
							.then(function (choferes) {
								resolve(choferes);
							}).catch(function (err) {
								reject(err);
							});
				})
				.catch(function (err) {
					reject(err);
				});
			});
		}
		function actualizarChofer(chofer) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.VarChar(50), chofer.idChofer)
					.input('nombre', mssql.VarChar(50), chofer.nombreChofer)
					.input('telefono', mssql.VarChar(50), chofer.telefono)
					.input('direccion', mssql.VarChar(50), chofer.direccion)
					.input('tipoLicencia', mssql.VarChar(50), chofer.tipoLicencia)
					.input('fechaVenceLicencia', mssql.DateTime(50), chofer.fechaVenceLicencia)
					.execute('pa_actualizarChoferes')
					.then(function () {
						console.log('editado');
						resolve();
					})
					.catch(function (err) {
						console.log('Esa no me la esperaba, un error', err);
						reject(err);
					});
				})
				.catch(function (err) {
					console.log('Esa no me la esperaba, un error', err);
					reject(err);
				});
			});
		}
		function eliminarChofer(chofer) {
			return $q(function(resolve, reject){
				mssql.connect(DatabaseConfig)
				.then(function () {
					new mssql.Request()
					.input('id', mssql.VarChar(50), chofer.idChofer)
					.execute('pa_eliminarChofer')
					.then(function (nada) {
						console.log(nada);
						resolve(nada);
					})
					.catch(function (err) {
						console.log('Esa no me la esperaba, error', err);
						reject(err);
					});
				})
				.catch(function (err) {
					console.log('acaso no la viste venir ? error', err);
					reject(err);
				});
			});
		}
		return {
			insertarChofer: insertarChofer,
			obtenerTodos: obtenerTodos,
			actualizarChofer: actualizarChofer,
			eliminarChofer: eliminarChofer,
			buscar: buscar
		}
	}
} ());
