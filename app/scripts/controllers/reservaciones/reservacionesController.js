(function(){
  'use strict';
  angular.module('core').controller('ReservacionesController', ReservacionesController);
  ReservacionesController.$inject = ['$scope', '$mdDialog', '$injector', 'DestinoService', 'BusetaService', 'ChoferService', 'HotelService','ReservacionService', 'ClientService', 'ViajesRealizadosService'];
  function ReservacionesController($scope, $mdDialog, $injector, DestinoService, BusetaService, ChoferService, HotelService, ReservacionService, ClientService, ViajesRealizadosService){
    var vm = this;
    vm.reservaciones = [];
    vm.reservacion = {}
    vm.destinos = [];
    vm.choferes = [];
    vm.busetas = [];
    vm.hoteles = [];
    vm.selected =[];
    vm.cliente = {};

    vm.obtenerTodasReservaciones = function(){
      ReservacionService.obtenerTodos()
      .then(function(reservaciones){
        vm.reservaciones = reservaciones;
      })
      .catch(function(err){
        console.log(err);
      });
    }
    vm.obtenerDeHoy = function(){
      ReservacionService.obtenerDeHoy()
      .then(function(reservaciones){
        vm.reservaciones = reservaciones;
      })
      .catch(function(err){
        console.log(err);
      });
    }
    vm.obtenerTodos = function(){
      /* caballada nivel dios, solo hacer en casos extremos :v */
      DestinoService.obtenerTodos()
      .then(function(destinos){
        vm.destinos = destinos;
        console.log(destinos);
        ChoferService.obtenerTodos()
        .then(function(choferes){
          vm.choferes = choferes;
          console.log(choferes);
          BusetaService.obtenerTodas()
          .then(function(busetas){
            vm.busetas = busetas;
            console.log('busetas', busetas);
            HotelService.obtenerTodos()
            .then(function(hoteles){
              vm.hoteles = hoteles;
              console.log('hoteles', hoteles);
              ReservacionService.obtenerPendientes()
              .then(function(reservaciones){
                vm.reservaciones = reservaciones;
              })
              .catch(function(err){
                console.log(err);
              });
            })
            .catch(function(err){
              console.log(err);
            });
          })
          .catch(function(err){
            console.log(err);
          });
        })
        .catch(function(err){
          console.log(err);
        });
      })
      .catch(function(err){
        console.log(err);
      });
    }
    vm.obtenerTodos();

    vm.verficarCliente = function(){
      $mdDialog.show({
        controller: infoClienteController,
        templateUrl: 'scripts/templates/reservaciones/infoCliente.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,
        locals: {
          data: angular.copy(vm.cliente)
        }
      }).then(function(data){
        ClientService.insertarCliente(data)
        .then(function(){
          vm.cliente = data;
          console.log('insertado');
        }).catch(function(err){
          console.log(err);
        });
      });
    }
    vm.registrarReservacion = function(reservacion){
      let alert = null;
      reservacion.idCliente = vm.cliente.idCliente;
      ReservacionService.insertarReservacion(reservacion)
      .then(function(){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Atención',
          content: 'La reservacion ha sido ingresada',
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
          $injector.get('$state').transitionTo('reservaciones.listar');
        });
      })//
      .catch(function(err){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'ERROR',
          content: 'Ha habido un error '+err,
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
        });
      });
    }
    vm.eliminarReservacion = function(){
      let reservacion = vm.selected[0];
      let confirm = $mdDialog.confirm()
      .title('Eliminar Reservacion')
      .textContent('Esta seguro?')
      .ariaLabel('Eliminar Reservacion')
      .ok('Eliminar')
      .cancel('Cancelar');
      $mdDialog.show(confirm).then(function(){
        ReservacionService.eliminarReservacion(reservacion)
        .then(function(){
          console.log('eliminado');
          vm.obtenerTodos();
          $mdDialog.hide();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al eliminar '+ err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      });
    }
    vm.cambiarARealizado = function(){
      let reservacion = vm.selected[0];
      let alert = null;
      $mdDialog.show({
        controller: viajesRealizadosController,
        templateUrl: 'scripts/templates/viajesRealizados/nuevo.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,
        locals: {
          reserva: angular.copy(reservacion)
        }
      }).then(function(viaje){
        ViajesRealizadosService.insertarViaje(viaje)
        .then(function(){
          alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'Atención',
            content: 'Reservación cambiada a Realizada',
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
            vm.obtenerTodos();
            vm.selected = [];
          })
        })
        .catch(function(err){
          alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          })
        });
      });
    }
    vm.editarReservacion = function(){
      let reservacion = vm.selected[0];
      ReservacionService.buscarReservacion(reservacion)
      .then(function(reserva){
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'scripts/templates/reservaciones/actualizar.tmpl.html',
          parent: angular.element(document.body),
          clickOutsideToClose:false,
          locals: {
            reserva: angular.copy(reserva[0][0]),
            hoteles: vm.hoteles,
            busetas: vm.busetas,
            choferes: vm.choferes,
            destinos: vm.destinos
          }
        }).then(function(reservacionEditar){
          ReservacionService.actualizarReservacion(reservacionEditar)
          .then(function(){
            vm.obtenerTodos();
          })
          .catch(function(err){
            let alert = $mdDialog.alert({
              clickOutsideToClose: true,
              escapeToClose: true,
              title: 'ERROR',
              content: 'Ha habido un error al Editar reservacion'+err,
              ok: 'Cerrar'
            });
            $mdDialog
            .show( alert )
            .finally(function() {
              alert = undefined;
            });
          });
        });
      }).catch(function(err){
        console.log(err);
      });
    }
    function DialogController($scope, $mdDialog, reserva, hoteles, busetas, choferes, destinos){
      $scope.data = {
        idReservacion: reserva.idReservacion,
        buseta: reserva.buseta,
        chofer: reserva.chofer,
        cantidadPersonas: parseInt(reserva.cantidadPersonas),
        precio: parseInt(reserva.precio),
        idHotel: reserva.idHotel,
        destino: reserva.destino,
        estado: reserva.estado,
        fechaHoraSalida: reserva.fechaHoraSalida,
        cliente: reserva.cliente
      };
      $scope.hoteles = hoteles;
      $scope.busetas = busetas;
      $scope.choferes = choferes;
      $scope.destinos = destinos;
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.cancel();
      }
    }
    function infoClienteController($scope, $mdDialog, data){
      $scope.data = data;
      $scope.found = false;
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.cancel();
      }
      $scope.buscarCliente = function(){
        ClientService.buscarCliente($scope.data)
        .then(function(cliente){
          console.log('CLiente encontrado', cliente[0][0]);
          if(cliente[0][0] != undefined){
            $scope.data = cliente[0][0];
            $scope.found = true;
          }else{
            $scope.found = false;
            $scope.data.idCliente == undefined ? $scope.data = {} : $scope.data.idCliente = $scope.data.idCliente
          }
        })
        .catch(function(err){
          console.log('error buscar cliente', err);
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al Editar reservacion'+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      }
    }
    function viajesRealizadosController($scope, $mdDialog, reserva){
      $scope.data = {};
      $scope.data.idReservacion = reserva.idReservacion;
      $scope.tipoDePagos = ['Efectivo', 'Tarjeta'];
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.cancel();
      }
    }
  }
}())
