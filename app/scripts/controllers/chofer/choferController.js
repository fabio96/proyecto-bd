(function(){
  'use strict';
  angular.module('core').controller('ChoferController', ChoferController);
  ChoferController.$inject = ['$scope', 'ChoferService', '$mdDialog', '$injector'];
  function ChoferController($scope, ChoferService, $mdDialog, $injector){
    var vm = this;
		vm.showSearch = false;
		vm.selected = [];
		vm.choferes = [];
		vm.chofer = {};
    //datos de chofer
    vm.obtenerTodos = function(){
      ChoferService.obtenerTodos()
      .then(function (choferes) {
        vm.choferes = choferes
        console.log(vm.choferes);
      })
      .catch(function (err) {
        console.log('error prro', err);
      });
    }
    vm.obtenerTodos();
    //empieza agregar chofer
    vm.registrar = function(chofer){
      let alert  = null;
      ChoferService.insertarChofer(chofer)
      .then(function(){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Atención ',
          content: 'El Chofer ha sido insertado',
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
          $injector.get('$state').transitionTo('chofer.listar');
        });
      }).catch(function(err){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Atención',
          content: 'Ha habido un error '+err,
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
        });
      });
    }
    vm.eliminarChofer = function(){
      let chofer = vm.selected[0];
      console.log('chofer es', chofer);
      let confirm = $mdDialog.confirm()
      .title('Eliminar Chofer')
      .textContent('Esta seguro?')
      .ariaLabel('Eliminar Chofer')
      .ok('Eliminar')
      .cancel('Cancelar');
      $mdDialog.show(confirm).then(function(){
        ChoferService.eliminarChofer(chofer).then(function(){
          console.log('elimiado');
          vm.obtenerTodos();
          $mdDialog.hide();
        }).catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al eliminar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      }, function(){
        $mdDialog.hide()
      });
    }
    //buscar
    vm.buscar = function(chofer){
      console.log(chofer);
      vm.datos = chofer;
      ChoferService.buscar(vm.datos);
    }

    vm.editarChofer = function(){
      let chofer = vm.selected[0];
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'scripts/templates/chofer/actualizar.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,
        locals: {
          data: angular.copy(chofer)
        }
      }).then(function(chofer){
        ChoferService.actualizarChofer(chofer).then(function(){
          vm.obtenerTodos();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al Editar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        }); //catch
      })
    }
    function DialogController($scope, $mdDialog, data){
      let fechaVenceLicencia = new Date(data.fechaVenceLicencia);
      $scope.data = data;
      $scope.data.fechaVenceLicencia =fechaVenceLicencia;
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.hide();
      }
    }
  }
}())
