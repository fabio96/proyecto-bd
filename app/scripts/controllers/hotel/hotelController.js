(function(){
  'use strict';
  angular.module('core').controller('HotelController', HotelController);
  HotelController.$inject = ['$scope', 'HotelService', '$mdDialog', '$injector'];
  function HotelController($scope, HotelService, $mdDialog, $injector){
		var vm = this;
		vm.showSearch = false;
		vm.selected = [];
		vm.hoteles = [];
		vm.hotel = {};
		//datos del hotel
		vm.obtenerTodos = function(){
			HotelService.obtenerTodos()
			.then(function(hoteles){
				vm.hoteles = hoteles;
				console.log(vm.hoteles);
			})
			.catch(function(err){
				console.log(err);
			});
		}
		vm.obtenerTodos();
		//registra hoteles
		vm.registrar = function(hotel){
			console.log(hotel);
			let alert = null;
			HotelService.insertarHotel(hotel)
			.then(function(){
				alert = $mdDialog.alert({
					clickOutSideToClose: true,
					escapeToClose: true,
					title: 'Atencion',
					content: 'Se ha registrado el hotel',
					ok: 'Cerrar'
				});
				$mdDialog
				.show(alert)
				.finally(function(){
					alert = undefined;
					$injector.get('$state').transitionTo('hoteles.listar');
				});
			}).catch(function(err){
				alert = $mdDialog.alert({
					clickOutSideToClose: true,
					escapeToClose: true,
					title: 'Atencion',
					content: 'Ha ocurrido un error'+err,
					ok: 'Cerrar'
				});
				$mdDialog
				.show(alert)
				.finally(function(){
					alert = undefined;
				});
			});
		}
		//eliminar hotel
		vm.eliminarHotel = function(){
			let hotel = vm.selected[0];
			console.log(hotel);
			let confirm = $mdDialog.confirm()
			.title('Eliminar Hotel')
			.textContent('Estas seguro ?')
			.ariaLabel('Eliminar Hotel')
			.ok('Eliminar')
			.cancel('Cancelar');
			$mdDialog.show(confirm).then(function(){
				HotelService.eliminarHotel(hotel).then(function(){
					console.log('eliminado');
					vm.obtenerTodos();
					$mdDialog.hide();
				}).catch(function(err){
					let alert = $mdDialog.alert({
						clickOutSideToClose: true,
						escapeToClose: true,
						title: 'ERROR',
						content: 'Ha ocurrido un error'+err,
						ok: 'Cerrar'
					});
					$mdDialog
					.show(alert)
					.finally(function(){
						alert = undefined;
					});
				});
			}, function(){
				$mdDialog.hide();
			});
		}
		//editar
		vm.actualizarHotel = function(){
			let hotel = vm.selected[0];
			$mdDialog.show({
				controller: DialogController,
				templateUrl: 'scripts/templates/hoteles/actualizar.tmpl.html',
				parent: angular.element(document.body),
				clickOutSideToClose: false,
				locals: {
					data: angular.copy(hotel)
				}
			}).then(function(hotel){
				HotelService.actualizarHotel(hotel).then(function(){
					vm.obtenerTodos();
				})
				.catch(function(err){
					let alert = $mdDialog.alert({
						clickOutSideToClose: true,
						escapeToClose: true,
						title: 'Error',
						content: 'Hay un error'+err,
						ok: 'Cerrar'
					});
					$mdDialog
					.show(alert)
					.finally(function(){
						alert = undefined;
					});
				});
			});
		}
		function DialogController($scope, $mdDialog, data){
			$scope.data = data;
			$scope.close = function(){
				$mdDialog.cancel();
			}
			$scope.editar = function(data){
				$mdDialog.hide(data);
			}
			$scope.cancel = function(){
				$mdDialog.hide();
			}
		}
  }
}())
