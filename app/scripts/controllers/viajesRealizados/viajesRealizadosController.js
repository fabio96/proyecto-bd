(function(){
  'use strict';
  angular.module('core').controller('ViajesRealizadosController', ViajesRealizadosController)
  ViajesRealizadosController.$inject = ['$scope', '$mdDialog', '$injector', 'ViajesRealizadosService'];
  function ViajesRealizadosController($scope, $mdDialog, $injector, ViajesRealizadosService){
    var vm = this;
    vm.selected = [];
    vm.viajes = [];
    vm.viaje = {};

    vm.obtenerTodos = function(){
      ViajesRealizadosService.obtenerTodos()
      .then(function(viajes){
        console.log('viajes', viajes);
        vm.viajes = viajes;
      })
      .catch(function(err){
        console.log('err al obtenerTodos', err);
      });
    }
    vm.obtenerTodos();

    vm.editarViaje = function(){
      let viaje = vm.selected[0];
      ViajesRealizadosService.buscarViaje(viaje)
      .then(function(viaje){
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'scripts/templates/viajesRealizados/actualizar.tmpl.html',
          parent: angular.element(document.body),
          clickOutsideToClose:false,
          locals: {
            data: angular.copy(viaje[0][0])
          }
        }).then(function(data){
          ViajesRealizadosService.actualizarViaje(data)
          .then(function(){
            vm.obtenerTodos();
          })
          .catch(function(err){
            let alert = $mdDialog.alert({
              clickOutsideToClose: true,
              escapeToClose: true,
              title: 'ERROR',
              content: 'Ha habido un error al Editar '+err,
              ok: 'Cerrar'
            });
            $mdDialog
            .show( alert )
            .finally(function() {
              alert = undefined;
            })
          });
        });
      })
      .catch();
    }
    vm.eliminarViaje = function(){
      let viaje = vm.selected[0];
      let confirm = $mdDialog.confirm()
      .title('Eliminar Viaje Realizado')
      .textContent('Esta seguro?')
      .ariaLabel('Eliminar Viaje')
      .ok('Eliminar')
      .cancel('Cancelar');
      $mdDialog.show(confirm)
      .then(function(){
        ViajesRealizadosService.eliminarViaje(viaje)
        .then(function(){
          vm.obtenerTodos();
          $mdDialog.hide();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al eliminar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      })
    }

    function DialogController($scope, $mdDialog, data){
      $scope.data = data;
      $scope.tipoDePagos = ['Efectivo', 'Tarjeta'];
      console.log('data', data);
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.cancel();
      }
    }
  }
}())
