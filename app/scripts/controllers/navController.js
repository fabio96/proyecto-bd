(function(){
  'use strict';
  var mssql = require('mssql');
  angular.module('core').controller('NavController', NavController);
  NavController.$inject = ['$scope', '$state', '$rootScope', 'DatabaseConfig', '$mdDialog'];
  function NavController($scope, $state, $rootScope, DatabaseConfig, $mdDialog){
    $scope.showNav = false;

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      if(toState.name === 'home'){
        $scope.showNav = false;
      }else{
        $scope.showNav = true;
      }
    });

    $scope.crearRespaldo = function(){
      mssql.connect(DatabaseConfig)
      .then(function(){
        new mssql.Request()
        .input('ruta', mssql.VarChar(20), "C:/sql server/respaldo.bak")
        .execute('pa_creacionBackup')
        .then(function(){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'Atencion',
            content: 'Se ha creado el Respaldo',
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        }).catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error  '+ err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });

      })
      .catch(function(err){
        let alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'ERROR',
          content: 'Ha habido un error'+ err,
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
        });
      });
    }
  }
}())
