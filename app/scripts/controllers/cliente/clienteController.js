(function () {
  'use strict';
  angular.module('core').controller('ClienteController', clienteController);
  clienteController.$inject = ['$scope', 'ClientService', '$mdDialog', '$injector'];
  function clienteController($scope, ClientService, $mdDialog, $injector) {
    let vm = this;
    vm.cliente = {};
    vm.clientes = [];
    vm.selected = [];
    vm.cliente = null;
    vm.showSearch = false;
    vm.obtenerTodos = function(){
      ClientService.obtenerTodos()
      .then(function (clientes) {
        vm.clientes = clientes;
        console.log(vm.clientes);
      })
      .catch(function (err) {
        console.log(err);
      });
    };
    vm.obtenerTodos();


    vm.insertarCliente = function(datos){
      let alert = null;
      ClientService.insertarCliente(datos)
      .then(function(cliente){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Attention',
          content: 'El cliente ha sido insertado',
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
          $injector.get('$state').transitionTo('clientes.listar');
        });
      }).catch(function(err){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Attention',
          content: 'Ha habid un error '+err,
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
        });
      });
    }

    vm.eliminarCliente = function(){
      let cl = vm.selected[0];
      let confirm = $mdDialog.confirm()
      .title('Eliminar Cliente')
      .textContent('Esta seguro?')
      .ariaLabel('Eliminar Cliente')
      .ok('Eliminar')
      .cancel('Cancelar');
      $mdDialog.show(confirm).then(function(){
        ClientService.eliminarCliente(cl).then(function(){
          console.log('elimiado');
          vm.obtenerTodos();
          $mdDialog.hide();
        }).catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al eliminar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      }, function(){
        $mdDialog.hide()
      });
    }

    vm.editarCliente = function(){
      let cl = vm.selected[0];
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'scripts/templates/clientes/actualizar.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,
        locals: {
          data: angular.copy(cl)
        }
      })
      .then(function(cliente) {
        ClientService.editarCliente(cliente).then(function(){
          vm.obtenerTodos();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al Editar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      });
    }
    function DialogController($scope, $mdDialog, data){
      $scope.data = data;
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.hide();
      }
    }

  }

} ())
