(function(){
  'use strict';
  angular.module('core').controller('BusetaController', BusetaController);
  BusetaController.$inject = ['$scope', 'BusetaService', '$injector', '$mdDialog'];
  function BusetaController($scope, BusetaService, $injector, $mdDialog){
    var vm = this;
    vm.showSearch = false;
    vm.selected = [];
    vm.busetas = [];
    vm.buseta = {};

    vm.obtenerTodas = function(){
      BusetaService.obtenerTodas()
      .then(function(busetas){
        vm.busetas = busetas;
      })
      .catch(function(err){
        console.log('Error al obtener todas las busetas '+err);
      });
    }
    vm.obtenerTodas();

    vm.insertarBuseta = function(buseta){
      BusetaService.insertarBuseta(buseta)
      .then(function(buseta){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Atención',
          content: 'La buseta ha sido insertada',
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
          $injector.get('$state').transitionTo('busetas.listar');
        });
      })
      .catch(function(err){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Atención',
          content: 'Ha habido un error '+err,
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
        });
      });
    }

    vm.eliminarBuseta = function(){
      let buseta = vm.selected[0];
      let confirm = $mdDialog.confirm()
      .title('Eliminar Buseta')
      .textContent('Esta seguro?')
      .ariaLabel('Eliminar buseta')
      .ok('Eliminar')
      .cancel('Cancelar');
      $mdDialog.show(confirm)
      .then(function(){
        BusetaService.eliminarBuseta(buseta)
        .then(function(){
          console.log('Buseta ELiminada');
          vm.obtenerTodas();
          $mdDialog.hide();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al eliminar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        }); // fin catch
      }); // then dialog
    } ///

    vm.editarBuseta = function(){
      let buseta = vm.selected[0];
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'scripts/templates/busetas/actualizar.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose: false,
        locals: {
          data: buseta
        }
      })
      .then(function(buseta){
        BusetaService.editarBuseta(buseta)
        .then(function(){
          vm.obtenerTodas();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al Editar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        }); //
      });
    }

    function DialogController($scope, $mdDialog, data){
      let annoCompra = new Date(data.annoCompra); // se hace casting data para ser desplegado en el datepicker
      let vence = new Date(data.fechaVenceRTV);
      $scope.data = data;
      $scope.data.fechaVenceRTV = vence;
      $scope.data.annoCompra = annoCompra;
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.hide();
      }
    }
  }
}())
