(function(){
	'use strict';
	var mssql = require('mssql');
  angular.module('core').controller('MainController', MainController);
  MainController.$inject = ['$scope', 'DatabaseConfig', '$injector', '$mdDialog'];
  function MainController($scope, DatabaseConfig, $injector, $mdDialog){
		var vm = this;
		vm.usuario ={};
		vm.iniciarSesion = function(){
			DatabaseConfig.user = vm.usuario.username;
			DatabaseConfig.password = vm.usuario.password;
			mssql.connect(DatabaseConfig)
			.then(function(){
				$injector.get('$state').transitionTo('reservaciones.listar')
			})
			.catch(function(err){
				let alert = $mdDialog.alert({
					clickOutsideToClose: true,
					escapeToClose: true,
					title: 'Atención',
					content: 'Ha habido un error '+err,
					ok: 'Cerrar'
				});
				$mdDialog
				.show( alert )
				.finally(function() {
					alert = undefined;
				})
			});
		}
  }
}())
