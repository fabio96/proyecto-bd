(function(){
  'use strict';
  angular.module('core').controller('DestinoController', DestinoController);
  DestinoController.$inject =['$scope', '$mdDialog', '$injector', 'DestinoService']
  function DestinoController($scope, $mdDialog, $injector, DestinoService){
    var vm = this;
    vm.showSearch = false;
    vm.selected = [];
    vm.desino = {};
    vm.destinos = [];
    vm.obtenerTodos = function(){
      DestinoService.obtenerTodos()
      .then(function(destinos){
        vm.destinos = destinos;
      })
      .catch(function(err){
        console.log('ha habido un error',err);
      });
    }
    vm.obtenerTodos();

    vm.registrarDestino = function(destino){
      let alert = null;
      DestinoService.insertarDestino(destino)
      .then(function(destino){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'Atención',
          content: 'El Destino ha sido insertado',
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
          $injector.get('$state').transitionTo('destino.listar');
        });
      })
      .catch(function(err){
        alert = $mdDialog.alert({
          clickOutsideToClose: true,
          escapeToClose: true,
          title: 'ERROR',
          content: 'Ha habido un error '+err,
          ok: 'Cerrar'
        });
        $mdDialog
        .show( alert )
        .finally(function() {
          alert = undefined;
        });
      });
    }

    vm.eliminarDestino = function(){
      let destino = vm.selected[0];
      let confirm = $mdDialog.confirm()
      .title('Eliminar Destino')
      .textContent('Esta seguro?')
      .ariaLabel('Eliminar Destino')
      .ok('Eliminar')
      .cancel('Cancelar');
      $mdDialog.show(confirm)
      .then(function(){
        DestinoService.eliminarDestino(destino)
        .then(function(){
          vm.obtenerTodos();
          $mdDialog.hide();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al eliminar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        });
      })
    }
    vm.editarDestino = function(){
      let destino = vm.selected[0];
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'scripts/templates/destino/actualizar.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,
        locals: {
          data: angular.copy(destino)
        }
      }).then(function(destino){
        DestinoService.editarDestino(destino)
        .then(function(){
          vm.obtenerTodos();
        })
        .catch(function(err){
          let alert = $mdDialog.alert({
            clickOutsideToClose: true,
            escapeToClose: true,
            title: 'ERROR',
            content: 'Ha habido un error al Editar '+err,
            ok: 'Cerrar'
          });
          $mdDialog
          .show( alert )
          .finally(function() {
            alert = undefined;
          });
        })
      }); //
    }
    function DialogController($scope, $mdDialog, data){
      $scope.data = data;
      $scope.close = function(){
        $mdDialog.cancel();
      }
      $scope.editar = function(data){
        $mdDialog.hide(data);
      }
      $scope.cancel = function(){
        $mdDialog.cancel();
      }
    }
  }
}())
